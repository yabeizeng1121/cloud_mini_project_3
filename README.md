# CDK TypeScript S3 Bucket Creation with AWS CodeWhisperer

## Overview
This project demonstrates the creation of an Amazon S3 bucket using the AWS Cloud Development Kit (CDK) along with AWS CodeWhisperer for generating CDK code. The S3 bucket is configured with advanced properties such as versioning and encryption to ensure data durability and security.

## Project Requirements

- **Create S3 bucket using AWS CDK:** Utilize AWS CDK to provision an S3 bucket on AWS.
- **Use CodeWhisperer to generate CDK code:** Leverage AWS CodeWhisperer for generating the necessary CDK code snippets.
- **Add bucket properties:** Configure the S3 bucket with versioning and encryption for enhanced security and data management.
- **Documentation:** Provide comprehensive documentation on the project setup, configuration, and deployment.

## Getting Started

### Prerequisites

- AWS Account
- AWS CLI installed and configured
- Node.js and npm installed
- AWS CDK installed
- Access to AWS CodeWhisperer

### Setup and Deployment

1. **Initialize CDK Project:**
   - Create a new directory for your project and navigate into it.
   - Initialize a new CDK project by running `cdk init app --language=typescript`.

2. **Generate CDK Code with CodeWhisperer:**
   - Use CodeWhisperer to generate the necessary CDK code for creating an S3 bucket with the desired properties (versioning and encryption).
   - Example prompts for CodeWhisperer: 
     - `create an S3 bucket with versioning enabled and S3-managed encryption`

3. **Configure S3 Bucket in CDK:**
   - Modify the generated CDK code to refine the S3 bucket configuration as needed.
   - Ensure versioning is enabled and encryption is set to S3-managed.

4. **Build and Deploy:**
   - Run `npm run build` to compile your TypeScript code.
   - Deploy your CDK stack to AWS by running `cdk deploy`.

## Project Deliverables

- **CDK App Code:** The TypeScript code for the CDK app is provided in the project repository.
- **Generated S3 Bucket Screenshot:** Screenshots demonstrating the created S3 bucket properties are included.
- **CodeWhisperer Writeup:** A detailed writeup explaining the use of AWS CodeWhisperer for generating CDK code is provided, highlighting the efficiency and effectiveness of the tool in automating code generation.

## Project Review
![Alt text](image.png)
![Alt text](image-1.png)
